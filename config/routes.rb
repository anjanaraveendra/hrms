Rails.application.routes.draw do
  get 'org_chart/org_chart'

  get 'add_documents/add_documents'

  get 'profile/profile'

  get 'attendance/index'

  get 'onboard/onboard'

  get 'employees/employees'

  get 'controller_name/page_name'

  get 'leaves/index'

  get 'home/index'

  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  devise_scope :user do
    authenticated :user do
      root 'home#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end

  resources :leaves, :employees, :onboard, :profile, :attendance
end
