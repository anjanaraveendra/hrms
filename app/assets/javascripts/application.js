// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery-1.12.4.min
//= require moment.min
//= require popper
//= require bootstrap
//= require pb.calendar
//= require jquery.easy-pie-chart
//= require bootstrap-datepicker/core
//= require bootstrap-select
//= orgchart.js
//= require custom
//= inercooler
//= require_tree .

$(document).ready(function () {

    $('.sub-menu-click').on('click', function () {
        $(this).toggleClass('clicked');
        $(this).parent().find('.sub-menu').toggle();
    });

    var current_yyyymm_ = moment().format("YYYYMM");

    $("#pb-calendar").pb_calendar({
        schedule_list: function (callback_, yyyymm_) {
            var temp_schedule_list_ = {};

            temp_schedule_list_[current_yyyymm_ + "03"] = [{
                'ID': 1,
                style: "red"
            }];

            temp_schedule_list_[current_yyyymm_ + "10"] = [{
                    'ID': 2,
                    style: "red"
                },
                {
                    'ID': 3,
                    style: "blue"
                },
            ];

            temp_schedule_list_[current_yyyymm_ + "20"] = [{
                    'ID': 4,
                    style: "red"
                },
                {
                    'ID': 5,
                    style: "blue"
                },
                {
                    'ID': 6,
                    style: "green"
                },
            ];
            callback_(temp_schedule_list_);
        },
        schedule_dot_item_render: function (dot_item_el_, schedule_data_) {
            dot_item_el_.addClass(schedule_data_['style'], true);
            return dot_item_el_;
        },

        'next_month_button': '<i class="la la-arrow-right"></i>',
        'prev_month_button': '<i class="la la-arrow-left"></i>',
    });

    $('.leave-count').easyPieChart({
        size: 120,
        lineCap: 'round',
        lineWidth: 10,
        barColor: '#1975ef',
        trackColor: '#f5f5f5',
        scaleColor: false,
        animate: 500
    });

    google.charts.load('current', {
        'packages': ['corechart']
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['Casual', 11],
            ['Sick', 2],
            ['Comp-Off', 2]
        ]);

        var options = {
            legend: 'none',
            colors: ['#1975ef', '#74acf6', '#b3d2fa'],
            pieSliceText: 'none',
            chartArea: {
                left: 5,
                top: 0,
                width: 125,
                height: 150
            }
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
    }

    $(function () {

        // We can attach the `fileselect` event to all file inputs on the page
        $(document).on('change', ':file', function () {
            var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
            input.trigger('fileselect', [numFiles, label]);
        });

        // We can watch for our custom `fileselect` event like this
        $(':file').on('fileselect', function (event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }

        });

        $.fn.selectpicker.Constructor.BootstrapVersion = '4';

        $('.selectpicker').selectpicker();

    });
});