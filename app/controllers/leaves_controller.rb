class LeavesController < ApplicationController
  def index
    @leaves = Leave.all
    @leave = Leave.new
  end

  def create
        binding.pry

    @leave = current_user.leaves.new(leave_params)
    if @leave.save
      redirect_to leaves_path
    else
      flash[:error] = "#{@leave.errors.full_messages}"
      redirect_to leaves_path
    end
  end

  private

  def leave_params
    params.require(:leave).permit!
  end
end
