class CreateLeaves < ActiveRecord::Migration[5.1]
  def change
    create_table :leaves do |t|
      t.integer :leave_type
      t.datetime :from_date
      t.datetime :to_date
      t.string :comments
      t.references :user, foreign_key: true
      t.integer :approved_by

      t.timestamps
    end
  end
end
