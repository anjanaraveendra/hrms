require 'test_helper'

class ControllerNameControllerTest < ActionDispatch::IntegrationTest
  test "should get page_name" do
    get controller_name_page_name_url
    assert_response :success
  end

end
